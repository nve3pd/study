import nativesockets
import strutils

var host = getHostByName("google.com")

proc getAddr(): string =
  result = ""
  for s in host.addrList:
    for i in countup(0, 3):
      result &= int(s[i]).intToStr & "."
  return result[0..(result.len - 2)]

echo getAddr()
