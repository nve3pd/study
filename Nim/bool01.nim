const
  zero = 0
  one = 1

if zero == 0:
  echo "ZERO"
else:
  echo "not ZERO"

if one.bool:  # bool(1)と同じ -> ただキャストしてるだけみたいなイメージ?
  echo "true"
else:
  echo "false"
