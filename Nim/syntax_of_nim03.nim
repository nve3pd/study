var a: int
echo a
a = 10
echo a

var s: string
echo s
s = "Hello"
echo s

var b: bool
echo b
if b:
  echo "True"
else:
  echo "False"
