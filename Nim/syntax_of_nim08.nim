proc fib(n: int): int =
  if n < 2:
    result = n
  else:
    result = fib(n - 1) + fib(n - 2)

echo fib(10)
echo 10.fib
echo 10.fib()

proc isPossible(x: int): bool = x >= 0

echo isPossible(10)
