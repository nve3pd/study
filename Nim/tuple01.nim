type t_tuple = tuple[name: string, data: int]

var a: t_tuple = (name: "hoge", data: 1)

echo a
a.name = "poyo"
echo a
