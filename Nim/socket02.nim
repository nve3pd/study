import strformat
import os
import net

let
  argv = os.commandLineParams()
  msg = fmt"GET / HTTP/1.1\nHost: {argv[0]}\r\nConnection: close\r\n\r\n"
  msg_len = len(msg)
var total_sent: int = 0
var s = net.newSocket()

proc main(): void =
  s.connect(argv[0])
  defer: s.close()
  
  while total_sent < msg_len:
    s.sendTo(argv[0], msg)

  var chunk: seq[int]

