import jester, asyncdispatch, json

routes:
  get "/":
    resp "Hello World"
  get "/users/@id":
    var data = %* {"id": @"id"}
    resp $data, "application/json"

runForever()
