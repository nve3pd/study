import sequtils

type Data = object
  name: string
  age: int

var s = @[
  Data(name: "hoge", age: 1),
  Data(name: "taro", age: 2),
  Data(name: "pi", age: 2),
  Data(name: "hoge", age: 43),
  Data(name: "fidsf", age: 234),
  Data(name: "hoge", age: 12315),
]

for i in filter(s, proc(x: Data): bool = x.name == "hoge"):
  echo i
