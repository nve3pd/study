import asynchttpserver
import asyncdispatch
import json

let server = newAsyncHttpServer()

proc handler(req: Request) {.async.} =
  if req.url.path == "/hello":
    let msg = %* {"message": "<script>alert(1)</script>"}
    let headers = newHttpHeaders([("Content-Type", "application/json")])
    await req.respond(Http200, $msg, headers)
  else:
    await req.respond(Http404, "Not Found")

waitFor server.serve(Port(8080), handler)
