proc name(a: int, b: string = "hoge"): string =
  result = ""
  for i in 1..a:
    result &= b

echo name(10)
echo name(10, "a")
echo name(10, b="hoge")
