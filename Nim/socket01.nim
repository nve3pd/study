import net

let msg: string = """
GET / HTTP/1.1\r\n
Host: github.com\r\n
Connection: close\r\n
"""

let
  msglen = len(msg)
  total_sent = 0

var socket = newSocket()
socket.connect("github.com", Port(80))

socket.send(msg)

let hoge = socket.recv(1024)

echo repr(hoge)

socket.close()
