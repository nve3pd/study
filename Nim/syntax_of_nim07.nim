block:
  var d: int = 10
#  echo d

# echo d   # error

block hoge:
  for i in 1..10:
    for j in 1..10:
      echo j
      if j == 2:
        break hoge  # block自体を抜けることができる
