import jester, asyncdispatch, json, htmlgen

routes:
  get "/":
    resp h1("Hello World!")

  get "/get/json":
    var s: string
    block:
      var f: File = open("hoge.json", FileMode.fmRead)
      defer: f.close()
      s = f.readAll()

    resp $parseJson(s)

runForever()
      
